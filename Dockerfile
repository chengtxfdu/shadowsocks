FROM alpine:latest

MAINTAINER Tingxian <chengtingxian@gmail.com>

WORKDIR /root

ENV PYTHONUNBUFFERED=1
RUN apk add --update --no-cache shadowsocks-libev
ADD config.json /root/config.json

EXPOSE 8388

CMD ["/usr/bin/ssserver", "-c", "/root/config.json"]
